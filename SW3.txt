* CREACIÓN DE LAS VLAN.

vlan database
vlan 99 name Usuarios_Gestion
vlan 50 name Usuarios_Ingenieria
vlan 100 name Usuarios_Tecnologia
exit

* CONFIGURACIÓN DE ENLACES TRUNK.

conf t 
interface fa 1/0
switchport mode access
switchport access vlan 50
interface fa 1/1
switchport mode access
switchport access vlan 100
end

config t 
interface fa 1/3 
switchport mode trunk
no shutdown
switchport trunk allowed vlan all
end

config t 
interface fa 1/2 
switchport mode trunk
no shutdown
switchport trunk allowed vlan all
end

* CONFIGURACIÓN DE LOS SPANNING-TREE.

config t 
spanning-tree vlan 99
spanning-tree vlan 100
spanning-tree vlan 50
end
