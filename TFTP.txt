* CONFIGURACIÓN DE LA DIRECCIÓN IP.

conf t
int f0/0
ip add 162.125.9.2 255.255.255.0
no sh 
exit

router rip
version 2
network 190.85.205.0
network 162.125.9.0
exit

* CONFIGURACIÓN DE LAS ACCESS-LIST.

access-list 10 deny 216.169.5.30 0.0.0.255
access-list 10 permit any
int f0/0
ip access-group 10 in