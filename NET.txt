* CONFIGURACIÓN DE LAS DIRECCIONES IP.

config t 
ipv6 unicast-routing
exit

conf t
interface serial 0/0
ipv6 address 2001:1200:C5:1::2/64
clock rate 64000
no shutdown
end

config t
int s 0/1
ipv6 address 2001:1200:C5:3::2/64
clock rate 64000
no shutdown
end
	
config t
int s0/2
ipv6 address 2001:1200:C5:2::2/64
clock rate 64000
no sh
end

* CONFIGURACIÓN DEL LOOPBACK.

conf t
int loopback 0
ip address 209.100.5.4 255.255.255.0
end

* CONFIGURACIÓN OSPF.

conf t
ipv6 router ospf 1
router-id 3.3.3.3
exit
int s0/0
ipv6 ospf 1 area 0
end

conf t
ipv6 router ospf 1
router-id 3.3.3.3
exit
int s0/2
ipv6 ospf 1 area 0
end

conf t
ipv6 router ospf 1
router-id 3.3.3.3
exit
int s0/1
ipv6 ospf 1 area 0
end
